<?php

namespace Auth\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

/**
 * Class JWTCreatedListener
 * @package Auth\EventListener
 */
class JWTCreatedListener
{
    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $payload = $event->getData();
        $user = $event->getUser();

        if (method_exists($user, 'getId')) {
            $payload['id'] = $user->getId();
        }
        $event->setData($payload);

        $header = $event->getHeader();
        $event->setHeader($header);
    }
}
