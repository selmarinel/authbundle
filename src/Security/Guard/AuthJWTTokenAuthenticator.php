<?php

namespace Auth\Security\Guard;

use Auth\Security\Token\AuthJWTUserToken;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthJWTTokenAuthenticator extends JWTTokenAuthenticator
{
    public function createAuthenticatedToken(UserInterface $user, $providerKey)
    {
        $authToken = parent::createAuthenticatedToken($user, $providerKey);

        return new AuthJWTUserToken(
            $authToken->getRoles(),
            $authToken->getUser(),
            $authToken->getCredentials(),
            $authToken->getProviderKey()
        );
    }
}